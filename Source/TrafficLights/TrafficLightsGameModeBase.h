// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TrafficLightsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TRAFFICLIGHTS_API ATrafficLightsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
