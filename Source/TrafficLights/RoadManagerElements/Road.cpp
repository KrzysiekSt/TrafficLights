﻿#include "Road.h"

Road::Road(TArray<USplineComponent*> InLines, UStaticMeshComponent* InTrafficLight,
           TArray<TArray<FVector>> InRoadRegions)
	: Lines(InLines),
	  TrafficLightMeshComponent(InTrafficLight),
	  RoadRegions(InRoadRegions)

{
	UMaterialInterface* CurrentMaterial = TrafficLightMeshComponent->GetMaterial(0);

	if (!CurrentMaterial->IsA<UMaterialInstanceDynamic>())
	{
		UMaterialInstanceDynamic* DynamicMaterial = UMaterialInstanceDynamic::Create(CurrentMaterial, nullptr);
		TrafficLightMeshComponent->SetMaterial(0, DynamicMaterial);
		LightSwitch = std::make_unique<TrafficLightSwitch>(DynamicMaterial, TrafficLightMeshComponent->GetWorld());
	}
	else
	{
		UMaterialInstanceDynamic* DynamicMaterial = Cast<UMaterialInstanceDynamic>(CurrentMaterial);
		LightSwitch = std::make_unique<TrafficLightSwitch>(DynamicMaterial, TrafficLightMeshComponent->GetWorld());
	}
}

void Road::DrawRoad(float time)
{
	for (auto& roadRegion : RoadRegions)
	{
		for (auto& point : roadRegion)
		{
			DrawDebugPoint(TrafficLightMeshComponent->GetWorld(), point, 5, colorForRoad, false, time);
		}
	}
}

void Road::DrawRoad(FColor color, float time)
{
	for (auto& roadRegion : RoadRegions)
	{
		for (auto& point : roadRegion)
		{
			DrawDebugPoint(TrafficLightMeshComponent->GetWorld(), point, 5, color, false, time);
		}
	}
}

void Road::DrawOverlappingRoads()
{
	for (auto& overlappingRoad : GetOverlappingRoads())
	{
		overlappingRoad.lock()->DrawRoad(FColor::Black);
	}
}

void Road::ChangeStateAndInformOthers(ETrafficLightState state,
                                      FTrafficLightTimes TrafficLightTimes,
                                      const std::function<void()>& callback)
{
	if (state == ETrafficLightState::BLINKING)
	{
		ChangeRoadState(state, TrafficLightTimes);
		MakeAllRoadsBlink(TrafficLightTimes);
		if (callback) callback();
	}
	else
	{
		standardLightsChange(state, TrafficLightTimes, callback);
	}
}

void Road::MakeAllRoadsBlink(FTrafficLightTimes TrafficLightTimes)
{
	for (auto& road : GetOverlappingRoads())
	{
		road.lock()->ChangeRoadState(ETrafficLightState::BLINKING, TrafficLightTimes);
	}

	for (auto& road : GetNotOverlappingRoads())
	{
		road.lock()->ChangeRoadState(ETrafficLightState::BLINKING, TrafficLightTimes);
	}
}

void Road::standardLightsChange(ETrafficLightState state,
                                FTrafficLightTimes TrafficLightTimes,
                                const std::function<void()>& callback)
{
	DrawRoad(FColor::Green);
	DrawOverlappingRoads();
	ChangeRoadState(state, TrafficLightTimes);
	ChangeOverlappingRoadsStage(TrafficLightTimes);
	ChangeNotOverlappingRoadsStage(TrafficLightTimes);
	if (callback) callback();
}


void Road::ChangeRoadState(ETrafficLightState state, FTrafficLightTimes TrafficLightTimes)
{
	LightSwitch->ChangeState(state, TrafficLightTimes);
	switch (state)
	{
	case ETrafficLightState::RED:
		ChangeLinesColor(FLinearColor::Red);
		DrawRoad(FColor::Red);
		break;
	case ETrafficLightState::BLINKING:
		ChangeLinesColor(FLinearColor::Yellow);
		DrawRoad(FColor::Yellow);
		break;
	case ETrafficLightState::GREEN:
		ChangeLinesColor(FLinearColor::Green);
		DrawRoad(FColor::Green);
		break;
	}
}

void Road::ChangeLinesColor(FLinearColor color)
{
	for (auto line : Lines)
	{
		line->EditorUnselectedSplineSegmentColor = color;
	}
}

void Road::ChangeOverlappingRoadsStage(FTrafficLightTimes TrafficLightTimes)
{
	for (auto& overlappingRoad : GetOverlappingRoads())
	{
		overlappingRoad.lock()->ChangeRoadState(ETrafficLightState::RED, TrafficLightTimes);
	}
}

void Road::ChangeNotOverlappingRoadsStage(FTrafficLightTimes TrafficLightTimes)
{
	for (auto& notOverlappingRoad : GetNotOverlappingRoads())
	{
		notOverlappingRoad.lock()->ChangeRoadState(ETrafficLightState::GREEN, TrafficLightTimes);
	}
}

void Road::DrawIndex(int index)
{
	DrawDebugString(TrafficLightMeshComponent->GetWorld(), Lines.Last()->GetComponentTransform().GetLocation(),
	                FString::FromInt(index), nullptr, FColor::Black, 1000000, false, 4);
}

Road::~Road()
{
	LightSwitch->Stop();
}
