﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "ETrafficLightState.generated.h"

UENUM(BlueprintType)
enum class ETrafficLightState : uint8
{
	GREEN UMETA(DisplayName = "Green"),
	RED UMETA(DisplayName = "Red"),
	BLINKING UMETA(DisplayName = "Blinking"),
};