﻿#pragma once

#include "CoreMinimal.h"
#include "ETrafficLightState.h"
#include "SplineComponentVisualizer.h"
#include "TrafficLightSwich/TrafficLightSwitch.h"
#include "TrafficLightSwich/TrafficLightTimes.h"


class TRAFFICLIGHTS_API Road
{
public:
	Road(TArray<USplineComponent*> InLines, UStaticMeshComponent* InTrafficLight, TArray<TArray<FVector>> InRoadRegions);
	~Road();

	bool IsOverlapping(Road* road);
	void DrawRoad(float time = 4.f);
	void DrawRoad(FColor color, float time = 4.f);
	void DrawOverlappingRoads();

	void ChangeStateAndInformOthers(ETrafficLightState state, FTrafficLightTimes TrafficLightTimes,
	                                const std::function<void()>& callback);
	void AddOverlappingRoad(const std::weak_ptr<Road>& road) { OverlappingRoads.push_back(road); }
	void AddNotOverlappingRoad(const std::weak_ptr<Road>& road) { NotOverlappingRoads.push_back(road); }
	const std::vector<std::weak_ptr<Road>>& GetOverlappingRoads() { return OverlappingRoads; }
	const std::vector<std::weak_ptr<Road>>& GetNotOverlappingRoads() { return NotOverlappingRoads; }
	const TArray<TArray<FVector>>& GetRoadRegions() { return RoadRegions; }
	void DrawIndex(int index);

private:
	TArray<USplineComponent*> Lines;
	UStaticMeshComponent* TrafficLightMeshComponent = nullptr;

	std::vector<std::weak_ptr<Road>> OverlappingRoads;
	std::vector<std::weak_ptr<Road>> NotOverlappingRoads;
	TArray<TArray<FVector>> RoadRegions;
	std::unique_ptr<TrafficLightSwitch> LightSwitch;
	FLinearColor linearColorForRoad = FLinearColor::MakeRandomColor();
	FColor colorForRoad = linearColorForRoad.ToFColor(true);


	void ChangeRoadState(ETrafficLightState state, FTrafficLightTimes TrafficLightTimes);
	void ChangeLinesColor(FLinearColor color);
	void ChangeOverlappingRoadsStage(FTrafficLightTimes TrafficLightTimes);
	void ChangeNotOverlappingRoadsStage(FTrafficLightTimes TrafficLightTimes);
	void standardLightsChange(ETrafficLightState state, FTrafficLightTimes TrafficLightTimes,
	                          const std::function<void()>& callback);
	void MakeAllRoadsBlink(FTrafficLightTimes TrafficLightTimes);
};
