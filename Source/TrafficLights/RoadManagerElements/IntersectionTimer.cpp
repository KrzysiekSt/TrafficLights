﻿#include "IntersectionTimer.h"



IntersectionTimer::IntersectionTimer(TArray<FTrafficLightSequence> InRoadQueue,
                                     std::vector<std::shared_ptr<Road>> InRoads,
                                     FTrafficLightTimes InTimes,
                                     UWorld* InWorld)
	: RoadQueue(InRoadQueue),
	  Roads(InRoads),
	  TrafficLightTimes(InTimes),
	  World(InWorld)

{
	sequenceIndex = 0;
}

void IntersectionTimer::start()
{
	sequenceIndex = 0;
	scheduleNextRoadChange();
}

void IntersectionTimer::scheduleNextRoadChange()
{
	if (RoadQueue.Num() == 0) return;
	sequenceIndex = sequenceIndex % RoadQueue.Num();

	const auto& sequence = RoadQueue[sequenceIndex];

	TimerDelegate.BindLambda([&]()
	{
		auto road = Roads[sequence.RoadIndex];
		UE_LOG(LogActorComponent, Warning, TEXT("Time: %f, Road: %i"),
		       World->GetTime().GetWorldTimeSeconds(), sequence.RoadIndex)
		road->ChangeStateAndInformOthers(sequence.State, TrafficLightTimes, [&]()
		{
			sequenceIndex++;
			scheduleNextRoadChange();
		});
	});

	World->GetTimerManager().SetTimer(Timer, TimerDelegate, sequence.LaunchAfterGivenSeconds, false);
}
