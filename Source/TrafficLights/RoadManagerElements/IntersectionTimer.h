﻿#pragma once
#include "CoreMinimal.h"
#include "FTrafficLightSequence.h"
#include "TrafficLightSwich/TrafficLightTimes.h"
#include "Road.h"

class TRAFFICLIGHTS_API IntersectionTimer
{
public:
	IntersectionTimer(){}

	IntersectionTimer(TArray<FTrafficLightSequence> InRoadQueue,
	                  std::vector<std::shared_ptr<Road>> InRoads,
	                  FTrafficLightTimes InTimes,
	                  UWorld* InWorld);
	void start();

private:
	void scheduleNextRoadChange();

	TArray<FTrafficLightSequence> RoadQueue;
	std::vector<std::shared_ptr<Road>> Roads;
	FTimerDelegate TimerDelegate;
	FTimerHandle Timer;
	int sequenceIndex;
	FTrafficLightTimes TrafficLightTimes;
	UWorld* World;
};
