﻿#pragma once
#include "ETrafficLightState.h"

#include "FTrafficLightSequence.generated.h"

USTRUCT(BlueprintType)
struct FTrafficLightSequence
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int RoadIndex;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LaunchAfterGivenSeconds;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ETrafficLightState State;
	
};