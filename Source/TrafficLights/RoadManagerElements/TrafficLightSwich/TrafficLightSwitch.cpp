﻿#include "TrafficLightSwitch.h"


TrafficLightSwitch::TrafficLightSwitch(UMaterialInstanceDynamic* MaterialInstance, UWorld* World)
	: MaterialInstance(MaterialInstance),
	  World(World)
{
	strategies.emplace_back(std::make_unique<RedToGreen>());
	strategies.emplace_back(std::make_unique<GreenToRed>());
	strategies.emplace_back(std::make_unique<Blinking>());
	strategies.emplace_back(std::make_unique<BlinkingToGreen>());
	strategies.emplace_back(std::make_unique<BlinkingToRed>());
	
	ChangeState(ETrafficLightState::BLINKING, FTrafficLightTimes());
}

void TrafficLightSwitch::Stop()
{
	for (auto& strategy : strategies)
	{
		strategy->stopExecuting(World);
	}
}

void TrafficLightSwitch::ChangeState(ETrafficLightState newState, FTrafficLightTimes TrafficLightTimes)
{
	for (auto& strategy : strategies)
	{
		strategy->stopExecuting(World);
		if (strategy->accept(currentState, newState)) //find_if -> else blinking
		{
			currentState = newState;
			strategy->execute(MaterialInstance, World, TrafficLightTimes);
		}
	}
}
