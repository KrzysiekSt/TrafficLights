﻿#pragma once
#include "TrafficLights/RoadManagerElements/TrafficLightSwich/TrafficLightTimes.h"
#include "TrafficLights/RoadManagerElements/TrafficLightSwich/TrafficLigthStrategy.h"

class TRAFFICLIGHTS_API GreenToRed : public TrafficLightStrategy
{
public:
	virtual bool accept(ETrafficLightState currentState, ETrafficLightState newState) override;
	virtual void execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes) override;

private:
	static void SwitchToYellow(UMaterialInstanceDynamic* MaterialInstance);
	static void SwitchToRed(UMaterialInstanceDynamic* MaterialInstance);
	void SwitchToRedAfterTime(UWorld* World, FTrafficLightTimes TrafficLightTimes);
};
