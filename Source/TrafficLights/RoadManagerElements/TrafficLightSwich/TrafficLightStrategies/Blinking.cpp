﻿#include "Blinking.h"

bool Blinking::accept(ETrafficLightState currentState, ETrafficLightState newState)
{
	return newState == ETrafficLightState::BLINKING;
}

void Blinking::execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	SmoorDelegate.BindLambda([=]()
	{
		Smoor(MaterialInstance);
	});
	
	TimerDelegate.BindLambda([=]()
	{
		Blink(MaterialInstance);
		World->GetTimerManager().SetTimer(SmoorTimer, SmoorDelegate, TrafficLightTimes.BlinkOffTime, false);
	});
	startBlinking(World, TrafficLightTimes);
}

void Blinking::stopExecuting(UWorld* World)
{
	TrafficLightStrategy::stopExecuting(World);
	World->GetTimerManager().PauseTimer(SmoorTimer);
}

void Blinking::startBlinking(UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	World->GetTimerManager().SetTimer(Timer, TimerDelegate, (TrafficLightTimes.BlinkTime + TrafficLightTimes.BlinkOffTime), true);
}

void Blinking::Blink(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 1.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 0.0f);
}

void Blinking::Smoor(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 0.0f);
}