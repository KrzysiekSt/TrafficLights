﻿#include "RedToGreen.h"

bool RedToGreen::accept(ETrafficLightState currentState, ETrafficLightState newState)
{
	return currentState == ETrafficLightState::RED && newState == ETrafficLightState::GREEN;
}

void RedToGreen::execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	TimerDelegate.BindLambda([=]()
	{
		SwitchToGreen(MaterialInstance);
	});
	SwitchToYellowAndRed(MaterialInstance);
	SwitchToGreenAfterTime(World, TrafficLightTimes);
}

void RedToGreen::SwitchToGreen(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 1.0f);
}

void RedToGreen::SwitchToYellowAndRed(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 1.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 1.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 0.0f);
}

void RedToGreen::SwitchToGreenAfterTime(UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	World->GetTimerManager().SetTimer(Timer, TimerDelegate, TrafficLightTimes.TimeForYellow, false);
}
