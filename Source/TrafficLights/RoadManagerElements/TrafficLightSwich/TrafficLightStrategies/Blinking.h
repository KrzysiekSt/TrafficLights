﻿#pragma once
#include "TrafficLights/RoadManagerElements/TrafficLightSwich/TrafficLightTimes.h"
#include "TrafficLights/RoadManagerElements/TrafficLightSwich/TrafficLigthStrategy.h"

class TRAFFICLIGHTS_API Blinking : public TrafficLightStrategy
{
public:
	virtual bool accept(ETrafficLightState currentState, ETrafficLightState newState) override;
	virtual void execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes) override;
	virtual void stopExecuting(UWorld* World) override;

private:
	void startBlinking(UWorld* World, FTrafficLightTimes TrafficLightTimes);
	static void Blink(UMaterialInstanceDynamic* MaterialInstance);
	static void Smoor(UMaterialInstanceDynamic* MaterialInstance);

	FTimerDelegate SmoorDelegate;
	FTimerHandle SmoorTimer;
};