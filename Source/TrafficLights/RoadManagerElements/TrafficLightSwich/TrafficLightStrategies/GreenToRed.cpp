﻿#include "GreenToRed.h"

bool GreenToRed::accept(ETrafficLightState currentState, ETrafficLightState newState)
{
	return currentState == ETrafficLightState::GREEN && newState == ETrafficLightState::RED;
}

void GreenToRed::execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	TimerDelegate.BindLambda([=]()
	{
		SwitchToRed(MaterialInstance);
	});
	SwitchToYellow(MaterialInstance);
	SwitchToRedAfterTime(World, TrafficLightTimes);
}

void GreenToRed::SwitchToYellow(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 1.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 0.0f);
}

void GreenToRed::SwitchToRed(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 1.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 0.0f);
}

void GreenToRed::SwitchToRedAfterTime(UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	World->GetTimerManager().SetTimer(Timer, TimerDelegate, TrafficLightTimes.TimeForYellow, false);
}
