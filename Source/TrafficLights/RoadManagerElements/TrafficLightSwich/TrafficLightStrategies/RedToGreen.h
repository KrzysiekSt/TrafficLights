﻿#pragma once
#include "TrafficLights/RoadManagerElements/TrafficLightSwich/TrafficLightTimes.h"
#include "TrafficLights/RoadManagerElements/TrafficLightSwich/TrafficLigthStrategy.h"

class TRAFFICLIGHTS_API RedToGreen : public TrafficLightStrategy
{
public:
	virtual bool accept(ETrafficLightState currentState, ETrafficLightState newState) override;
	virtual void execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes) override;

private:
	static void SwitchToGreen(UMaterialInstanceDynamic* MaterialInstance);
	static void SwitchToYellowAndRed(UMaterialInstanceDynamic* MaterialInstance);
	void SwitchToGreenAfterTime(UWorld* World, FTrafficLightTimes TrafficLightTimes);
};
