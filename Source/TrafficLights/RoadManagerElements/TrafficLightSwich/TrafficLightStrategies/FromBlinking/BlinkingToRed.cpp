﻿#include "BlinkingToRed.h"

bool BlinkingToRed::accept(ETrafficLightState currentState, ETrafficLightState newState)
{
	return currentState == ETrafficLightState::BLINKING && newState == ETrafficLightState::RED;
}

void BlinkingToRed::execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	SwitchToRed(MaterialInstance);
}

void BlinkingToRed::SwitchToRed(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 1.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 0.0f);
}