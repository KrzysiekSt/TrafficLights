﻿#include "BlinkingToGreen.h"

bool BlinkingToGreen::accept(ETrafficLightState currentState, ETrafficLightState newState)
{
	return currentState == ETrafficLightState::BLINKING && newState == ETrafficLightState::GREEN;
}

void BlinkingToGreen::execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes)
{
	SwitchToGreen(MaterialInstance);
}

void BlinkingToGreen::SwitchToGreen(UMaterialInstanceDynamic* MaterialInstance)
{
	MaterialInstance->SetScalarParameterValue("RedLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("YellowLightIntensity", 0.0f);
	MaterialInstance->SetScalarParameterValue("GreenLightIntensity", 1.0f);
}
