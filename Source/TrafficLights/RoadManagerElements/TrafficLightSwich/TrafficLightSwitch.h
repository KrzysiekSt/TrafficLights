﻿#pragma once
#include "TrafficLightTimes.h"
#include "TrafficLigthStrategy.h"
#include "TrafficLightStrategies/Blinking.h"
#include "TrafficLightStrategies/GreenToRed.h"
#include "TrafficLightStrategies/RedToGreen.h"
#include "TrafficLightStrategies/FromBlinking/BlinkingToGreen.h"
#include "TrafficLightStrategies/FromBlinking/BlinkingToRed.h"

class TRAFFICLIGHTS_API TrafficLightSwitch
{
public:
	TrafficLightSwitch(UMaterialInstanceDynamic* MaterialInstance, UWorld* World);

	TrafficLightSwitch(const TrafficLightSwitch&) = delete;
	TrafficLightSwitch& operator=(const TrafficLightSwitch&) = delete;
	void Stop();

	TrafficLightSwitch(TrafficLightSwitch&&) noexcept = default;
	TrafficLightSwitch& operator=(TrafficLightSwitch&&) noexcept = default;
	void ChangeState(ETrafficLightState newState, FTrafficLightTimes TrafficLightTimes);

private:
	UMaterialInstanceDynamic* MaterialInstance;
	std::vector<std::unique_ptr<TrafficLightStrategy>> strategies;
	ETrafficLightState currentState = ETrafficLightState::BLINKING;
	UWorld* World;
};
