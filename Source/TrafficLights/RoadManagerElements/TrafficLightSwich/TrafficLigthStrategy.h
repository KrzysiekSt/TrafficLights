﻿#pragma once
#include "TrafficLights/RoadManagerElements/ETrafficLightState.h"

class TRAFFICLIGHTS_API TrafficLightStrategy
{
public:
	virtual ~TrafficLightStrategy() = default;

	virtual bool accept(ETrafficLightState currentState, ETrafficLightState newState) = 0;
	virtual void execute(UMaterialInstanceDynamic* MaterialInstance, UWorld* World, FTrafficLightTimes TrafficLightTimes) = 0;
	virtual void stopExecuting(UWorld* World) {if (World->IsValidLowLevelFast()) World->GetTimerManager().PauseTimer(Timer);}

protected:
	FTimerDelegate TimerDelegate;
	FTimerHandle Timer;
};
