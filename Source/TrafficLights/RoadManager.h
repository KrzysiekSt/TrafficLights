﻿#pragma once
#include <limits>
#include "CoreMinimal.h"
#include "RoadManagerElements/ETrafficLightState.h"
#include "RoadManagerElements/FTrafficLightSequence.h"
#include "RoadManagerElements/Road.h"
#include "RoadManagerElements/IntersectionTimer.h"
#include "VectorTypes.h"
#include "Algo/MaxElement.h"
#include "Components/ActorComponent.h"
#include "Components/SplineComponent.h"
#include "Components/StaticMeshComponent.h"
#include "RoadManagerElements/TrafficLightSwich/TrafficLightTimes.h"

#include "RoadManager.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TRAFFICLIGHTS_API URoadManager : public UActorComponent
{
	GENERATED_BODY()

public:
	URoadManager();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(CallInEditor, Category="Functions")
	void BuildRoads();

	UFUNCTION(CallInEditor, Category="Functions")
	void MarkRoad();

	UFUNCTION(CallInEditor, Category="Functions")
	void MarkOverlappingToRoad();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	int RoadCheckIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config",
		meta = (ClampMin = "0.0", ClampMax = "100.0", UIMin = "0.0", UIMax = "100.0"))
	float ParallelismThreshold = 80.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config",
		meta = (ClampMin = "0.0", ClampMax = "10000.0", UIMin = "0.0", UIMax = "10000.0"))
	float RoadWidth = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	FTrafficLightTimes TrafficLightTimes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	TArray<FTrafficLightSequence> RoadQueue;

protected:
	virtual void BeginPlay() override;

private:
	std::vector<std::shared_ptr<Road>> Roads;

	TArray<USplineComponent*> Lines;
	TArray<UStaticMeshComponent*> TrafficLights;
	TArray<TArray<float>> SplinesParallelismMatrix;
	AActor* Intersection;

	FTimerHandle Timer;
	FTimerDelegate TimerDelegate;
	IntersectionTimer Intersection_Timer;

	void RemoveOtherMeshesFrom(TArray<UStaticMeshComponent*>& Array);
	TArray<TTuple<USplineComponent*, UStaticMeshComponent*>> FindRoads();
	USplineComponent* FindMainLineForTrafficLight(UStaticMeshComponent* trafficLight);
	USplineComponent* FindLineUnderTheTrafficLight(const FVector& trafficLightLocation);
	int TryFindIndexOfParallelPair(int indexOfLineWeNeedToAttach,
	                               int indexOfLineWeAreChecking,
	                               const TArray<TTuple<USplineComponent*, UStaticMeshComponent*>>& pairs);
	float CalculateMedianDistance(USplineComponent* Spline1, USplineComponent* Spline2, int numSamples);
	int TryFindBestParallelPair(USplineComponent* lineToAttach,
	                            const TArray<TTuple<USplineComponent*, UStaticMeshComponent*>>& pairs);
	void CreateRoads();
	TArray<int> SortRestOfTheLinesBetweenPairs(TArray<USplineComponent*> linesToBringUnderTrafficLights,
	                                           TArray<TTuple<USplineComponent*, UStaticMeshComponent*>> linesAndTrafficLightsPairs);
	void CreateRoad(const TArray<USplineComponent*>& linesToBringUnderTrafficLights,
	                TArray<TTuple<USplineComponent*, UStaticMeshComponent*>> linesAndTrafficLightsPairs,
	                TArray<int> indexesToPair,
	                int indexOfPair);
	TArray<FVector> FindClosestPointsOnLines(const FVector& point);
	TArray<FVector> CreateRoadRegion(int lineIndex);
	TArray<FVector> CreateRoadRegion(USplineComponent* line);
	bool IsPointInsideRegion(const FVector& point, const TArray<FVector>& polygonPoints);
	TArray<TArray<float>> CreateMatrixWithParallelismValues();
	float CalculateParallelism(USplineComponent* first, USplineComponent* second);

	void CreateCollisionArrays();
};
