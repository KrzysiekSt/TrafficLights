﻿#include "RoadManager.h"

#include <algorithm>

URoadManager::URoadManager()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void URoadManager::BeginPlay()
{
	Super::BeginPlay();
	BuildRoads();

	Intersection_Timer = IntersectionTimer(RoadQueue, Roads, TrafficLightTimes, GetWorld());
	Intersection_Timer.start();
}


void URoadManager::BuildRoads()
{
	Roads.clear();
	Intersection = GetOwner();

	Intersection->GetComponents(Lines);
	Intersection->GetComponents(TrafficLights);

	RemoveOtherMeshesFrom(TrafficLights);

	SplinesParallelismMatrix = CreateMatrixWithParallelismValues();

	Roads.reserve(TrafficLights.Num());
	CreateRoads();
	for (int i = 0; i < Roads.size(); i++)
	{
		Roads[i]->DrawIndex(i);
	}
	
	CreateCollisionArrays();
}

void URoadManager::RemoveOtherMeshesFrom(TArray<UStaticMeshComponent*>& Array)
{
	auto notTrafficLights = [](UStaticMeshComponent* component) -> bool
	{
		int MinusOneIfNotLight = component->GetStaticMesh().Get()->GetName().Find("TrafficLights");
		return MinusOneIfNotLight == -1;
	};

	Array.RemoveAll(notTrafficLights);
}

TArray<TArray<float>> URoadManager::CreateMatrixWithParallelismValues()
{
	/*
	  Parallelism Matrix :
	  -------------------------
		   |  A  |  B  |  C  |  D  |
	  -------------------------
	  A  |  -  |  1  |  2  |  3  |
	  B  |  1  |  -  |  4  |  5  |
	  C  |  2  |  4  |  -  |  6  |
	  D  |  3  |  5  |  6  |  -  |
	  -------------------------

	  Explanation:
	  - The matrix represents values between elements A, B, C, D...
	  - The diagonal values are represented by '-' because we are looking for two parallel lines
	  - Off-diagonal values represent the connections or weights between the elements.
	  - For example, the value at row A, column B is 1, meaning there is a connection with a value of 1 from A to B.
	  - values are mirrored through diagonal
	*/

	TArray<TArray<float>> theMatrix;
	theMatrix.SetNum(Lines.Num());
	for (auto& row : theMatrix)
	{
		row.SetNum(Lines.Num());
	}

	// Calculate the values
	for (int i = 0; i < Lines.Num(); ++i)
	{
		for (int j = 0; j < Lines.Num(); ++j)
		{
			if (i != j)
			{
				float value = CalculateParallelism(Lines[i], Lines[j]);
				(value > ParallelismThreshold) ? theMatrix[i][j] = value : theMatrix[i][j] = 0;
			}
		}
	}

	// force diagonal as 0
	for (int i = 0; i < Lines.Num(); i++)
	{
		theMatrix[i][i] = 0;
	}

	// print
	FString Output = TEXT("\nParallelism Matrix :\n");
	for (int32 i = 0; i < theMatrix.Num(); ++i)
	{
		Output += FString::Printf(TEXT("%2i\t\t|"), i + 1);
	}
	Output += FString::Printf(TEXT("\n-----------------------------------\n"));

	for (int32 i = 0; i < theMatrix.Num(); ++i)
	{
		for (int32 j = 0; j < theMatrix.Num(); ++j)
		{
			Output += FString::Printf(TEXT("\t%2.0f\t|"), theMatrix[i][j]);
		}
		Output += FString::Printf(TEXT("\t%2i\n"), i + 1);
	}
	UE_LOG(LogTemp, Log, TEXT("%s"), *Output);

	return theMatrix;
}

void URoadManager::CreateRoads()
{
	TArray<USplineComponent*> linesToBringUnderTrafficLights = Lines;
	TArray<TTuple<USplineComponent*, UStaticMeshComponent*>> linesAndTrafficLightsPairs = FindRoads();
	for (auto [lineAlreadyUnderTrafficLight, __] : linesAndTrafficLightsPairs)
	{
		linesToBringUnderTrafficLights.Remove(lineAlreadyUnderTrafficLight);
	}

	TArray<int> indexesToPair = SortRestOfTheLinesBetweenPairs(linesToBringUnderTrafficLights,
	                                                           linesAndTrafficLightsPairs);

	for (int indexOfPair = 0; indexOfPair < linesAndTrafficLightsPairs.Num(); indexOfPair++)
	{
		CreateRoad(linesToBringUnderTrafficLights, linesAndTrafficLightsPairs, indexesToPair, indexOfPair);
	}
}

TArray<TTuple<USplineComponent*, UStaticMeshComponent*>> URoadManager::FindRoads()
{
	TArray<TTuple<USplineComponent*, UStaticMeshComponent*>> LinesAndTrafficLightsPairs;
	for (auto trafficLight : TrafficLights)
	{
		USplineComponent* mainLine = FindMainLineForTrafficLight(trafficLight);
		if (!mainLine)
		{
			UE_LOG(LogActorComponent, Error, TEXT("Failed to get Light and line pair!!!"))
		}
		LinesAndTrafficLightsPairs.Add({mainLine, trafficLight});
	}

	return LinesAndTrafficLightsPairs;
}

USplineComponent* URoadManager::FindMainLineForTrafficLight(UStaticMeshComponent* trafficLight)
{
	// opis 
	int greaterThanMinusOneIfLeft = trafficLight->GetStaticMesh().Get()->GetName().Find("TrafficLights_L");
	int greaterThanMinusOneIfRight = trafficLight->GetStaticMesh().Get()->GetName().Find("TrafficLights_R");
	FVector offset;

	if (greaterThanMinusOneIfLeft > -1)
	{
		offset = {0, -350, 0};
	}
	else if (greaterThanMinusOneIfRight > -1)
	{
		offset = {0, 350, 0};
	}
	else
	{
		UE_LOG(LogActorComponent, Error, TEXT("Some invalid component here. We should do someting!!!"))
	}

	const FVector trafficLightLocation = trafficLight->GetComponentTransform().GetLocation() +
		trafficLight->GetComponentTransform().GetRotation().RotateVector(offset);


	DrawDebugPoint(GetWorld(), trafficLightLocation, 20, FColor::Blue, false, 100000);

	USplineComponent* mainLine = FindLineUnderTheTrafficLight(trafficLightLocation);

	return mainLine;
}

USplineComponent* URoadManager::FindLineUnderTheTrafficLight(const FVector& trafficLightLocation)
{
	for (auto line : Lines)
	{
		TArray<FVector> regionEdgePoints = CreateRoadRegion(line);
		if (IsPointInsideRegion(trafficLightLocation, regionEdgePoints))
		{
			return line;
		}
	}

	return nullptr;
}

bool URoadManager::IsPointInsideRegion(const FVector& point, const TArray<FVector>& polygonPoints)
{
	int32 numPoints = polygonPoints.Num();
	bool bInside = false;
	for (int32 i = 0, j = numPoints - 1; i < numPoints; j = i++)
	{
		const FVector& Pi = polygonPoints[i];
		const FVector& Pj = polygonPoints[j];

		if (((Pi.Y > point.Y) != (Pj.Y > point.Y)) &&
			(point.X < (Pj.X - Pi.X) * (point.Y - Pi.Y) / (Pj.Y - Pi.Y) + Pi.X))
		{
			bInside = !bInside;
		}
	}
	return bInside;
}

TArray<int> URoadManager::SortRestOfTheLinesBetweenPairs(TArray<USplineComponent*> linesToBringUnderTrafficLights,
                                                         TArray<TTuple<USplineComponent*, UStaticMeshComponent*>>
                                                         linesAndTrafficLightsPairs)
{
	TArray<int> indexesToPair;
	indexesToPair.Reserve(linesToBringUnderTrafficLights.Num());
	for (auto lineToAttach : linesToBringUnderTrafficLights)
	{
		int pairIndex = TryFindBestParallelPair(lineToAttach, linesAndTrafficLightsPairs);
		if (pairIndex == -1)
		{
			UE_LOG(LogActorComponent, Error, TEXT("Line without the traffic light"))
		}
		indexesToPair.Add(pairIndex);
	}
	return indexesToPair;
}

int URoadManager::TryFindBestParallelPair(USplineComponent* lineToAttach,
                                          const TArray<TTuple<USplineComponent*, UStaticMeshComponent*>>& pairs)
{
	int result = -1;
	float closestDistance = std::numeric_limits<float>::max();
	int indexOfLineWeNeedToAttach = Lines.Find(lineToAttach);

	for (int indexOfLineWeAreChecking = 0; indexOfLineWeAreChecking < SplinesParallelismMatrix[
		     indexOfLineWeNeedToAttach].Num(); indexOfLineWeAreChecking++)
	{
		int possiblePairIndex = TryFindIndexOfParallelPair(indexOfLineWeNeedToAttach, indexOfLineWeAreChecking, pairs);

		if (possiblePairIndex != -1)
		{
			float distanceToMatch = CalculateMedianDistance(Lines[indexOfLineWeNeedToAttach],
														Lines[indexOfLineWeAreChecking], 100);
			UE_LOG(LogActorComponent, Warning, TEXT("closestDistance: %f, distanceToMatch: %f "), closestDistance, distanceToMatch)
			if (closestDistance > distanceToMatch)
			{
				closestDistance = distanceToMatch;
				result = possiblePairIndex;
			}
		}
	}
	

	return result;
}

int URoadManager::TryFindIndexOfParallelPair(int indexOfLineWeNeedToAttach, int indexOfLineWeAreChecking,
                                             const TArray<TTuple<USplineComponent*, UStaticMeshComponent*>>& pairs)
{
	float valueOfParallelism = SplinesParallelismMatrix[indexOfLineWeNeedToAttach][indexOfLineWeAreChecking];
	auto parallelLine = [=](TTuple<USplineComponent*, UStaticMeshComponent*> item)
	{
		auto [line, _] = item;
		return Lines[indexOfLineWeAreChecking] == line;
	};
	bool IsParallel = valueOfParallelism > ParallelismThreshold;
	bool IsMainLine = pairs.ContainsByPredicate(parallelLine);
	// UE_LOG(LogActorComponent, Warning, TEXT("Attaching: %s, To: %s "), *Lines[indexOfLineWeNeedToAttach]->GetName(), *Lines[indexOfLineWeAreChecking]->GetName())
	// UE_LOG(LogActorComponent, Warning, TEXT("b1: %s, b2: %s "), IsParallel ? TEXT("true") : TEXT("false"), IsMainLine ? TEXT("true") : TEXT("false"))
	if(IsParallel && IsMainLine)
	{
		auto item = pairs.FindByPredicate(parallelLine);
		return pairs.Find(*item);
	}
	return -1;
}

float URoadManager::CalculateMedianDistance(USplineComponent* Spline1, USplineComponent* Spline2, int numSamples = 100)
{
	if (!Spline1 || !Spline2)
	{
		UE_LOG(LogTemp, Warning, TEXT("Invalid spline component."));
		return -1.0f;
	}

	float Length1 = Spline1->GetSplineLength();
	float Length2 = Spline2->GetSplineLength();

	std::vector<float> distances;

	for (int i = 0; i < numSamples; i++)
	{
		float distanceAlongFirstLine = (i / static_cast<float>(numSamples - 1)) * Length1;
		float distanceAlongSecondLine = (i / static_cast<float>(numSamples - 1)) * Length2;

		FVector pointOnTheFirstLine = Spline1->GetLocationAtDistanceAlongSpline(
			distanceAlongFirstLine, ESplineCoordinateSpace::World);
		FVector pointOnTheSecondLine = Spline2->GetLocationAtDistanceAlongSpline(
			distanceAlongSecondLine, ESplineCoordinateSpace::World);

		float distanceBetweenTheLines = FVector::Dist(pointOnTheFirstLine, pointOnTheSecondLine);
		distances.push_back(distanceBetweenTheLines);
	}

	std::sort(distances.begin(), distances.end());

	float medianDistance;
	int middleIndex = distances.size() / 2;

	(distances.size() % 2 == 0)
		? medianDistance = (distances[middleIndex] + distances[middleIndex + 1]) / 2.0f
		: medianDistance = distances[middleIndex];

	return medianDistance;
}

void URoadManager::CreateRoad(const TArray<USplineComponent*>& linesToBringUnderTrafficLights,
                              TArray<TTuple<USplineComponent*, UStaticMeshComponent*>> linesAndTrafficLightsPairs,
                              TArray<int> indexesToPair, int indexOfPair)
{
	TArray<USplineComponent*> attachedTrafficLines;
	for (int indexOfLineToAttach = 0; indexOfLineToAttach < linesToBringUnderTrafficLights.Num(); indexOfLineToAttach++)
	{
		if (indexesToPair[indexOfLineToAttach] == indexOfPair)
		{
			attachedTrafficLines.Add(linesToBringUnderTrafficLights[indexOfLineToAttach]);
		}
	}

	auto [mainLine, trafficLight] = linesAndTrafficLightsPairs[indexOfPair];
	attachedTrafficLines.Add(mainLine);

	TArray<TArray<FVector>> roadRegions;

	for (auto trafficLine : attachedTrafficLines)
	{
		if (trafficLine) roadRegions.Add(CreateRoadRegion(trafficLine));
	}
	if (attachedTrafficLines.Num() != 0 && roadRegions.Num() == attachedTrafficLines.Num())
	{
		Roads.emplace_back(std::make_shared<Road>(attachedTrafficLines, trafficLight, roadRegions));
	}
	else UE_LOG(LogActorComponent, Error, TEXT("Some road not created for %s!!!"), *trafficLight->GetName())
}

TArray<FVector> URoadManager::CreateRoadRegion(int lineIndex)
{
	return CreateRoadRegion(Lines[lineIndex]);
}

TArray<FVector> URoadManager::CreateRoadRegion(USplineComponent* line)
{
	TArray<FVector> leftEdgePoints;
	TArray<FVector> rightEdgePoints;
	int numSamples = 100;


	for (int i = 0; i <= numSamples; ++i)
	{
		float sampleValue = static_cast<float>(i) / numSamples;
		FVector pointOnSpline = line->GetLocationAtTime(
			sampleValue, ESplineCoordinateSpace::World, true);

		FVector pointDirection = line->GetDirectionAtTime(sampleValue, ESplineCoordinateSpace::Type::World, true);
		FVector leftPerpendicularDirection = pointDirection.RotateAngleAxis(-90, FVector::UnitZ());
		FVector rightPerpendicularDirection = pointDirection.RotateAngleAxis(90, FVector::UnitZ());

		FVector leftEdgePoint = pointOnSpline + (leftPerpendicularDirection.GetSafeNormal() * RoadWidth);
		FVector rightEdgePoint = pointOnSpline + (rightPerpendicularDirection.GetSafeNormal() * RoadWidth);

		leftEdgePoints.Add(leftEdgePoint);
		rightEdgePoints.Add(rightEdgePoint);
	}

	TArray<FVector> polygonPoints;
	for (int i = 0; i < numSamples; ++i)
	{
		polygonPoints.Add(leftEdgePoints[i]);
	}
	for (int i = numSamples - 1; i >= 0; --i)
	{
		polygonPoints.Add(rightEdgePoints[i]);
	}

	//print
	FColor color = FColor::MakeRandomColor();
	for (auto& point : polygonPoints)
	{
		DrawDebugPoint(GetWorld(), point, 20, color, false, 10.f);
	}

	return polygonPoints;
}

float URoadManager::CalculateParallelism(USplineComponent* first, USplineComponent* second)
{
	const int32 numSamples = 100;
	float totalParallelism = 0.0f;

	for (int32 i = 0; i < numSamples; ++i)
	{
		float sampleValue = i / static_cast<float>(numSamples - 1);

		FVector Tangent1 = first->GetTangentAtDistanceAlongSpline(first->GetSplineLength() * sampleValue,
		                                                          ESplineCoordinateSpace::World).GetSafeNormal();
		FVector Tangent2 = second->GetTangentAtDistanceAlongSpline(second->GetSplineLength() * sampleValue,
		                                                           ESplineCoordinateSpace::World).GetSafeNormal();

		float dotProduct = FVector::DotProduct(Tangent1, Tangent2);
		float angle = FMath::Acos(dotProduct) * 180.0f / PI;

		float percentageOfParallelismAtPointsOnTheLine = 1.0f - (angle / 90.0f);
		totalParallelism += percentageOfParallelismAtPointsOnTheLine;
	}

	return (totalParallelism / numSamples) * 100.0f;
}

void URoadManager::CreateCollisionArrays()
{
	for (auto& road : Roads)
	{
		for (auto& anotherRoad : Roads)
		{
			if (&road == &anotherRoad) continue;

			bool colliding = false;
			for (auto& roadRegion : road->GetRoadRegions())
				for (auto& anotherRoadRegion : anotherRoad->GetRoadRegions())
					for (auto& anotherRoadEdgePoint : anotherRoadRegion)
					{
						if (IsPointInsideRegion(anotherRoadEdgePoint, roadRegion))
						{
							colliding = true;
							break;
						}
					}

			if (colliding) road->AddOverlappingRoad(anotherRoad);
			else road->AddNotOverlappingRoad(anotherRoad);
		}
	}
}


void URoadManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void URoadManager::MarkRoad()
{
	if (Roads.size() > RoadCheckIndex) Roads[RoadCheckIndex]->DrawRoad(FColor::Magenta);
}

void URoadManager::MarkOverlappingToRoad()
{
	if (Roads.size() > RoadCheckIndex) Roads[RoadCheckIndex]->DrawOverlappingRoads();
}
