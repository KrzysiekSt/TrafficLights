// Copyright Epic Games, Inc. All Rights Reserved.

#include "TrafficLights.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TrafficLights, "TrafficLights" );
